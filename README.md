# Advanced Audio Visual Processing - Courseworks 01


## Nathan Adams / nadam001@gold.ac.uk
 
 Experiment to do the following
 
 1. Output a 3rd Order Ambisonics stream which can be decoded by something like the IEM AIIRADecode plugin
 2. Creating multiple orbiting sound sources mving in 3D space
 3. each orbiting object playing a wavetable sound based on some sort of rhythm
 
 For creation of the 3rd Order stream, I am using the [calculations at](http://pcfarina.eng.unipr.it/Aurora/HOA_explicit_formulas.htm)
 
 The theory for this exercise is based on an Immersive Audio Workshop given by
 [Amoenus](https://amoenus.co.uk) @ Iklectoic in March 2022
 
