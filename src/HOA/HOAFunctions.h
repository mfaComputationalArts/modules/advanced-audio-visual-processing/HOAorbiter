//
//  HOAFunctions.h
//  loopbackOutput
//
//  Created by lxinspc on 25/03/2022.
//

#pragma once

#include "ofMain.h"

class HOA {
  
public:
  
  enum Order {
    FIRST,
    SECOND,
    THIRD
  };
  
  HOA(Order o = THIRD) : _order(o) {
    switch (o) {
      case FIRST:
        _nChannels = 1;
        break;
      case SECOND:
        _nChannels = 8;
        break;
      case THIRD:
        _nChannels = 16;
        break;
    }
  };
  
  struct Spherical {
    float azimuth;
    float elevation;
    float r;
  };
  
  
  void encodeSample(double v, size_t i, const Spherical& s, ofSoundBuffer& output);
  void encodeSample(double v, size_t i, const glm::vec3& p, ofSoundBuffer& output);

  
  
  double gainForChannel(int c, const Spherical& s);
  double gainForChannel(int c, const glm::vec3& p);
  
  //Utility functions
  static glm::vec3 sphericalToCartesian(const Spherical& s);
  
  
  
  
  
private:
  
  Order _order;
  size_t _nChannels;
  
  //Precalc sqrts used a lot in the calculations
  const double ROOT_3_4 = sqrt(3.0 / 4.0);
  const double ROOT_15_4 = sqrt(15.0 / 4.0);
  const double ROOT_3_8 = sqrt(3.0 / 8.0);
  const double ROOT_5_8 = sqrt(5.0 / 8.0);
  
  const double ROOT_3 = sqrt(3.0);
  const double ROOT_15 = sqrt(15.0);
  
};



