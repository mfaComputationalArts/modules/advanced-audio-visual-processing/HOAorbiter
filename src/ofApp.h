/**
 
 # Advanced Audio Visual Processing - Courseworks 01
 
 ## Nathan Adams / nadam001@gold.ac.uk
 
 Experiment to do the following
 
 1. Output a 3rd Order Ambisonics stream which can be decoded by something like the IEM AIIRADecode plugin
 2. Creating multiple orbiting sound sources mving in 3D space
 3. each orbiting object playing a wavetable sound based on some sort of rhythm
 
 For creation of the 3rd Order stream, I am using the calculations at
 
 http://pcfarina.eng.unipr.it/Aurora/HOA_explicit_formulas.htm
 
 The theory for this exercise is based on an Immersive Audio Workshop given by
 Amoenus @ Iklectoic in March 2022
 
 https://amoenus.co.uk
 
 */

#pragma once


//Open Frameworks Includes
#include "ofMain.h"

//Add on includes
#include "maximilian.h"

//Local Includes
#include "HOAFunctions.h"
#include "Orbiter.h"
#include "ofxWLHalColor.h"
#include "BufferClock.h"

class ofApp : public ofBaseApp{
  
public:
  void setup();
  void update();
  void draw();

  //Listener events
  void onKeyPressed(int key);
  
  //Callback function for audio out
  void audioOut(ofSoundBuffer& output);

private:
  
  //We use Rogue Amoeba's Loopback to create a 16 channel device we can send sound to
  //this is then picked up in Max so that we can apply the chosen decoder (binaural for headphones
  //of IEM AIIRADecode for a space with multi-speaker setup
  const string DEVICE_NAME = "Rogue Amoeba Software, Inc.: Ambisonic3";

  //Device settings
  const size_t SAMPLE_RATE = 48000;
  const size_t NUMBER_OUT_CHANNELS = 16;
  const size_t BUFFER_SIZE = 512;
  
  const size_t BUFFER_N = NUMBER_OUT_CHANNELS * BUFFER_SIZE;
    
  const double BPM = 160.0;
  const int COUNT = 32;
  const double GATE = 0.25;
  const int TICKS_PER_BEAT = 2;
  
  //Sound stream
  ofSoundStream _soundStream;
  
  //Setup sound stream
  bool _setupSoundstream();
  
  //HOA Class for gain calculations (this defaults to third order calculations, and => 16 channels of audio)
  HOA _hoa;
  
  //Orbiters
  typedef vector<Orbiter> _Orbiters;
  _Orbiters _orbiters;
  
  const size_t NUM_ORBITERS = 2;
  
  void _setupOrbiters();
  
  
  
  //Orbiter visualisation
  ofEasyCam _camera;
  
  const float SCALE = 300.0f;
  
  ofSpherePrimitive _bounds;
  ofColor _boundsColor;
  
  bool _displayBounds = true;
  
  //Color
  HAL2001 _hal2001;
  
  
  //Global timing for sound playback
  BufferClock _clock;
  
  
};
