//
//  Orbiter.h
//  HOAOrbiter
//
//  Created by lxinspc on 26/03/2022.
//

#pragma once

//Openframeworks includes
#include "ofMain.h"

//Add On Includes
#include "maximilian.h"

//Local Includes
#include "HOAFunctions.h"         //include to get the spherical definition
#include "ofxWLHALColor.h"

class Orbiter {
  
public:

  struct Polar {
    float theta;
    float r;
  };
  
  typedef vector<int> PlayAt;
  
  void setup(Polar p, Polar dP, glm::vec3 r, PlayAt at, size_t gateSamples, double weight, size_t buffer_size);
  
  void update();
  
  HOA::Spherical getPosition();
  
  bool playAtBeat(int b);
  
  void draw();
  
  void fillSampleBuffer(ofSoundBuffer& output);
  void fillSampleBuffer(ofSoundBuffer& output, size_t startAt);
  
private:
  
  //Sample rate, so we can donecessary calculations for playing sounds
  size_t _sampleRate;
  
  ofFloatColor _color;
  
  HAL2001 _halColor;
  
  PlayAt _at;
  bool _playing;
  
  Polar _position;     //Current spherical coordinates
  Polar _dP;           //Change per frame

  glm::mat3 _rMtx;      //Rotation Matrix
  
  void _setupRotationMatrix(glm::vec3 r);
  
  glm::vec3 _positionXYZ;
  
  //HOA::Spherical _cartesianToSpherical(const glm::vec3& _cartesian);
  
  HOA _hoa;
  
  //Define a maxiOsc for the sound for this
  maxiOsc _osc;
  
  //Frequency
  double _f = 220.0;
  
  //Gate samples defines how many samples need to be played per sound;
  size_t _gateSamples;
  
  //gateRemain - defines how many samples were outside the buffer, so need to played when the next buffer is filled
  size_t _gateRemain;
  
  //Weight, how much we add to each sample
  double _weight;
  
  //Buffer size - saves doing some calculation
  size_t _bufferSize;
  
};
