//
//  Orbiter.cpp
//  HOAOrbiter
//
//  Created by lxinspc on 26/03/2022.
//

#include "Orbiter.h"

void Orbiter::setup(Polar p, Polar dP, glm::vec3 r, PlayAt at, size_t gateSamples, double weight, size_t buffer_size) {
  
  //Setup with a start polar coordiate (p), the change per frame to the polar coordiant (dP)
  //and an euler angle for rotating a 2D orbit
  _position = p;
  _dP = dP;
  //Set up a glm::mat3 matrix we can apply to the calculated points in time
  _setupRotationMatrix(r);
 
  //allocate a random color from the HAL2001 palette
  _color = _halColor.getRandom();
  
  //play at tells us which beat something will be played on
  _at = at;
  
  //Store the nuymber of samples we need to play when being played
  _gateSamples = gateSamples;
  
  _weight = weight;
  _bufferSize = buffer_size;
  
  _f += ofRandom(512.0);
  
}

void Orbiter::update() {
  
  //Update theta and radius by the delta value
  _position.theta += _dP.theta;
  _position.r += _dP.r;

  //Wrap theta between 0 and 2 PI if required
  if (_position.theta > TWO_PI) {
    _position.theta -= TWO_PI;
  }
  
  if (_position.theta < 0) {
    _position.theta += TWO_PI;
  }
  
  //Work out cartesian position, by converting to XY cartesian, then applying the rotation transformation
  _positionXYZ.x = cos(_position.theta) * _position.r;
  _positionXYZ.y = sin(_position.theta) * _position.r;
  _positionXYZ.z = 0.0f;                                //Make sure Z is 0 prior to the transform
  
  _positionXYZ = _rMtx * _positionXYZ;
  
}


bool Orbiter::playAtBeat(int b) {
  
  PlayAt::iterator it = find(_at.begin(), _at.end(), b);
  _playing = it != _at.end();
  return _playing;
  
}


void Orbiter::draw() {
  
  _color.a = _playing ? 1.0f : 0.3f;
  
  ofSetColor(_color);
  ofDrawSphere(_positionXYZ, 0.03);
  
}

#pragma mark - Sound Processing

//This currently feels clunky as I attempt to deal with the breaking up into buffer chunks!

void Orbiter::fillSampleBuffer(ofSoundBuffer& output) {
  
  //this is called at the beginning of any buffer, and is filled with any remaining samples
  if (_gateRemain > 0) {
    size_t n = MIN(_gateRemain, _bufferSize);
    for (size_t i = 0; i < n; i++) {
      _hoa.encodeSample((_osc.saw(_f) * _weight), i, _positionXYZ, output);
    }
    _gateRemain -= n;
  }
  
}


void Orbiter::fillSampleBuffer(ofSoundBuffer& output, size_t startAt) {
  
  //this is called to say a sound has started playing at the specified point
  size_t endAt = startAt + _gateSamples;
  for (int i = 0; i < _bufferSize; i++) {
    if (i > startAt && i < endAt) {
      _hoa.encodeSample((_osc.saw(_f) * _weight), i, _positionXYZ, output);
    }
  }
  if (endAt > _bufferSize) {
    //this is probabaly always true as we are always going to be playing for longer than
    //the buffer time, but we can check anyway, and update the remaining samples that will need
    //to be filled in the next buffer
    _gateRemain = _gateSamples - startAt;
  }
  
}



void Orbiter::_setupRotationMatrix(glm::vec3 r) {
  
  //We want to create the apporpiate mat4 transformation for three angles around the X,Y,Z axis
  float cA = cos(r.z);
  float sA = sin(r.z);
  float cB = cos(r.y);
  float sB = sin(r.y);
  float cG = cos(r.x);
  float sG = sin(r.x);

  _rMtx = glm::mat3({
    cA * cB, sA * cB, -sB,
    cA * sB * sG - sA * cG, sA * sB * sG + cA * cG, cB * sG,
    cA * sB * cG + sA * sG, sA * sB * cG - cA * sG, cB * cG
  });
  
}
