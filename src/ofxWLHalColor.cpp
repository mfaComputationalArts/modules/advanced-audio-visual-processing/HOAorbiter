//
//  ofxWLHalColor.cpp
//  HOAOrbiter
//
//  Created by lxinspc on 28/03/2022.
//

#include "ofxWLHalColor.h"

HAL2001::HAL2001() {
  
  _colors = {
    { ATM_01, ofFloatColor(0.61f, 0.02f, 0.20f) },
    { ATM_03, ofFloatColor(0.19f, 0.33f, 0.54f) },
    { ATM_04, ofFloatColor(0.16f, 0.36f, 0.64f) },
    { ATM_05, ofFloatColor(0.11f, 0.44f, 0.58f) },
    { ATM_06, ofFloatColor(0.16f, 0.35f, 0.35f) },
    { ATM_07, ofFloatColor(0.44f, 0.36f, 0.45f) },
    { ATM_08, ofFloatColor(0.14f, 0.33f, 0.56f) },
    { CNT_01, ofFloatColor(0.26f, 0.43f, 0.29f) },
    { CNT_02, ofFloatColor(0.13f, 0.26f, 0.20f) },
    { CNT_03, ofFloatColor(0.31f, 0.27f, 0.54f) },
    { CNT_04, ofFloatColor(0.40f, 0.24f, 0.64f) },
    { CNT_05, ofFloatColor(0.36f, 0.49f, 0.58f) },
    { CNT_06, ofFloatColor(0.40f, 0.19f, 0.35f) },
    { CNT_07, ofFloatColor(0.20f, 0.33f, 0.45f) },
    { CNT_08, ofFloatColor(0.03f, 0.28f, 0.56f) },
    { COM_01, ofFloatColor(0.45f, 0.23f, 0.38f) },
    { COM_02, ofFloatColor(0.45f, 0.23f, 0.38f) },
    { COM_03, ofFloatColor(0.08f, 0.42f, 0.69f) },
    { COM_04, ofFloatColor(0.05f, 0.24f, 0.38f) },
    { COM_05, ofFloatColor(0.47f, 0.34f, 0.68f) },
    { COM_08, ofFloatColor(0.13f, 0.13f, 0.17f) },
    { COM_11, ofFloatColor(0.25f, 0.24f, 0.27f) },
    { COM_19, ofFloatColor(0.53f, 0.40f, 0.53f) },
    { COM_10, ofFloatColor(0.00f, 0.10f, 0.00f) }
  };
  
}

ofFloatColor HAL2001::get(Name n) {
  
  if (_colors.count(n) == 0) {
    n = ATM_04;
  }
  return _colors[n];
  
}

ofFloatColor HAL2001::getRandom() {
  
  //For time being we don't want to return COM_10 as is used for backgrounds - so we just look
  //for a vlaue 1 less than the size
  return _colors[static_cast<Name>((int)ofRandom(_colors.size() - 1))];
  
}
