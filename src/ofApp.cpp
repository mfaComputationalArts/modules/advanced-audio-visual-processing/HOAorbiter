#include "ofApp.h"

void ofApp::setup(){

  //Setup the soundstream to output audio to
  _setupSoundstream();
  
  _setupOrbiters();
  
  ofBackground(_hal2001.get(HAL2001::COM_10));
  
  _bounds.setRadius(1.0f);
  
  _boundsColor = ofColor::aliceBlue;
  _boundsColor.a = 80;

  
  //Clock setup
  
  BufferClock::Options o;
  
  o.bpm = BPM;
  o.ticksPerBeat = TICKS_PER_BEAT;
  o.bufferSize = BUFFER_SIZE;
  o.count = COUNT;                     //Cout up to 16, which we will use as a pattern length
  
  _clock.setup(o);
  
}

void ofApp::update(){

  //Orbiter positions are updated in audioOut, so that we b=make sure we have the
  //corret position when each sample is calculated
    
}

void ofApp::draw(){
    
  //Draw Orbiters
  _camera.begin();

  
  ofPushMatrix();
  //The HOA calculations all work in a space where Z is height, and Y is depth
  //whereas Openframeworks uses Y as height and Z as depth
  //so we rotate the world by HALF_PI on the X axis so we get the same coordinate spaces
  ofRotateXRad(HALF_PI);
  
  ofScale(SCALE);

  if (_displayBounds) {
    ofSetColor(_boundsColor);
    _bounds.drawWireframe();
  }

  for (_Orbiters::iterator it = _orbiters.begin(); it != _orbiters.end(); ++it) {
    it->draw();
  }
  
  ofPopMatrix();
  
  _camera.end();
  
  
  
  //Draw gain levels
  
}

#pragma mark - Soundstream setup and callbacks

bool ofApp::_setupSoundstream() {
  
  //Try to setup the soundstream based on the specifed device name - if we can't find
  //it then we will stop the application
  
  //Get a list of all the devices available
  vector<ofSoundDevice> devices = _soundStream.getDeviceList();
  
  vector<ofSoundDevice>::iterator it = find_if(devices.begin(), devices.end(), [&](const ofSoundDevice& d){
    return d.name == DEVICE_NAME;
  });
  
  
  
  if (it == devices.end()) {
    cout << "Could not find device " << DEVICE_NAME << " check loop back is running and virtual device has the right name" << endl;
    return false;
  } else {
    cout << "Found device " << DEVICE_NAME << " will connect and setup" << endl;

    ofSoundStreamSettings settings;
    settings.setOutDevice(*(it));
    settings.setOutListener(this);
    settings.sampleRate = SAMPLE_RATE;
    settings.numOutputChannels = NUMBER_OUT_CHANNELS;
    settings.bufferSize = BUFFER_SIZE;
    
    _soundStream.setup(settings);
    
    return true;
  }

}

void ofApp::audioOut(ofSoundBuffer &output) {
    
  BufferClock::Ticks _ticks;
  _clock.tick(_ticks);

  //0 output across the board so samples * number out channels = 0
//  memset(&output, 0, BUFFER_N);

  //First update all the positions of the orbiters
  for (_Orbiters::iterator it = _orbiters.begin(); it != _orbiters.end(); ++it) {
    it->update();
    
    //Now fill any remaining samples from previous slices, if required
    it->fillSampleBuffer(output);
    
    //Lets fill the buffer with any playing orbiters
    for (BufferClock::Ticks::iterator tt = _ticks.begin(); tt != _ticks.end(); ++tt) {
      if (it->playAtBeat(tt->count)) {
        it->fillSampleBuffer(output, tt->count);
      }
    }
  }
  
}


#pragma mark - Setup orbiters

void ofApp::_setupOrbiters() {
  
  _orbiters.resize(NUM_ORBITERS);
  
  //Need to calculate a gate time, and number of samples this means we produce sound for
  
  double _playTime = (1.0 / (BPM / 60.0)) * GATE;
  size_t _gateSamples = SAMPLE_RATE * _playTime;
  
  cout << "play time is " << ofToString(_playTime) << "s, which is " << _gateSamples << " samples" << endl;
  
  double _weight = 1.0 / (double)NUM_ORBITERS;
  
  for (_Orbiters::iterator it = _orbiters.begin(); it != _orbiters.end(); ++it) {
    
    Orbiter::Polar p;
    p.theta = ofRandom(TWO_PI);
    p.r = 1.0f;
    
    Orbiter::Polar dP;
    dP.theta = ofRandom(1.0f) / 300.0f;
    dP.r = 0;
    
    //Create a rotation to apply to the 2D orbit
    glm::vec3 r;

    r.x = ofRandom(TWO_PI);
    r.y = ofRandom(TWO_PI);
    r.z = ofRandom(TWO_PI);

    //Set a random play at pattern - first decide a number of notes, between 3 and 8
    
    Orbiter::PlayAt _at;
    int notes = 3 + ofRandom(9);
    int note = (int)ofRandom(COUNT / notes);
    for (int i = 0; i < notes; i++) {
      _at.push_back(note);
      note += (int)ofRandom(COUNT * 1.5 / notes);
      note %= COUNT;
    }
    
    it->setup(p, dP, r, _at, _gateSamples, _weight, BUFFER_SIZE);
    
  }
  
  
}


