//
//  ColorPalette.h
//  alephNull
//
//  Created by lxinspc on 17/08/2021.
//

#pragma once

#include "ofMain.h"


class HAL2001 {
  
public:
  
  enum Name {
    ATM_01,
    ATM_03,
    ATM_04,
    ATM_05,
    ATM_06,
    ATM_07,
    ATM_08,
    CNT_01,
    CNT_02,
    CNT_03,
    CNT_04,
    CNT_05,
    CNT_06,
    CNT_07,
    CNT_08,
    COM_01,
    COM_02,
    COM_03,
    COM_04,
    COM_05,
    COM_08,
    COM_10,
    COM_11,
    COM_19
  };
  
  HAL2001();
  
  ofFloatColor get(Name n);
  ofFloatColor getRandom();
  
private:
  
  typedef map<Name, ofFloatColor> _Map;
  
  _Map _colors;

  
};
