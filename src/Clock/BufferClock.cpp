//
//  BufferClock.cpp
//  HOAOrbiter
//
//  Created by lxinspc on 28/03/2022.
//

#include "BufferClock.h"

void BufferClock::setup(Options o) {
  
  _bufferSize = o.bufferSize;
  _bpm = o.bpm;
  _ticksPerBeat = o.ticksPerBeat;
  
  _count = o.count;
  _currentCount = 0;
  
  _bps = _bpm / 60.0 * _ticksPerBeat;
  
}

bool BufferClock::tick(Ticks& at) {
  
  bool r = false;
  
  //Check tick status for all samples in the buffer size, we are assuing that we could
  //get mutiple ticks per buffer, so using a vector to return them
  at.clear();
  
  for (size_t i = 0; i < _bufferSize; i++) {
    
    int tick = floor(_timer.phasor(_bps));
    
    if (tick > 0) {
      
//      cout << "tick " << _currentCount << " @sample " << i << endl;
      
      r = true;
      at.push_back({_currentCount, i});
  
      _currentCount++;
      _currentCount %= _count;
      
      
    }
  }
  
  return r;
  
}


