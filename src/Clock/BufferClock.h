/**
 
 # Buffer Clock
 
 ## Replacement for the maximilian maxiClock object
   
 As we are using a buffer, we aren't calling an audio out function for every sample - we are
 are calling for every n Samples where n is the biffer size
 
 This is a simple replacement for the maxiClock object which updates acoording to the sample size
 and returns not just if a tick happened, but at which sample, and if it is still ongoing
 into the next sample
 
 I could have wrapped a maxiClock in a loop for X number of samples, but thought would be instructive
 to also understand how the maxiClock object works, and try for myself
 
 I could have also embedded this into the loop which will produce the necessary samples, calling
 maxiClock directly -
 
 */

#pragma once


//Open Frameworks Includes
#include "ofMain.h"

//Add on includes
#include "maximilian.h"

//Local Includes



//maxiClock();
//void ticker();
//void setTempo(double bpm);
//void setTicksPerBeat(int ticksPerBeat);
//maxiOsc timer;
//int currentCount;
//int lastCount;
//int playHead;
//double bps;
//double bpm;
//int ticks;
//bool tick;

/**
 
 # Some thoughts on timing and sequencer resolution
 
 A fully featured version of this might aim to use a sequencer resolution of 960PPQN, which means
 
 at 120bpm, and 4 ticks per beat - i.e. 8bps, this means we have 125ms in which 960 pulses should occur
 which is 0.13ms per pulse
 
 if we have a buffer size of 512 samples at 48000Hz same sample rate, then a full sample buffer
 occupies approx 106ms - this means there could be multiple events within a single buffer event
 
 As this is only going to be used to sequence say semi-quavers - i.e. 4 ticks per beat, then we have
 172bpm at 4 ticks per beat ~= 11.47bps - 87ms then we still want to think about returning multiple
 ticks per check
 
 */



class BufferClock {
  
public:
  
  struct Options {
    size_t bufferSize;
    double bpm;
    int ticksPerBeat;
    int count;
  };
  
  void setup(Options o);
 
  struct Tick {
    int count;
    size_t at;
  };
  
  typedef vector<Tick> Ticks;
  
  bool tick(Ticks& at);
  
private:
  
  size_t _bufferSize;
  double _bpm;
  double _bps;
  int _ticksPerBeat;
  
  int _count;
  int _currentCount;
  
  maxiOsc _timer;           //like maxiClock, we'll use this as the timing object
  
  
};
